import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {UserService} from '../../shared/services/user.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AdminAuthGuardService implements CanActivate {

    constructor(private authService: AuthService, private userService: UserService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authService.appUser$
            .pipe(
                map(user => {
                    if (user.isAdmin === true) {
                        return true;
                    }

                    this.router.navigate(['\login'], {
                        queryParams: {
                            returnUrl: state.url
                        }
                    });

                    return false;
                })
            );
    }
}
