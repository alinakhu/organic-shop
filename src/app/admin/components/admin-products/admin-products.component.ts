import {Component, OnInit, OnDestroy} from '@angular/core';
import {IProduct} from '../../../shared/models/product';
import {Subscription} from 'rxjs';
import {ProductService} from '../../../shared/services/product.service';

@Component({
    selector: 'app-admin-products',
    templateUrl: './admin-products.component.html',
    styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit, OnDestroy {

    productSubscription: Subscription;
    items: IProduct[] = [];

    constructor(private productService: ProductService) { }

    ngOnInit() {
        this.productSubscription = this.productService.getAll()
            .subscribe(products => {
                this.items = products;
            });
    }

    ngOnDestroy() {
        this.productSubscription.unsubscribe();
    }
}
