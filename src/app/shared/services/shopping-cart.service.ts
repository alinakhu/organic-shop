import {ShoppingCart} from '../models/shopping-cart';
import {IProduct} from '../models/product';
import {AngularFireDatabase} from 'angularfire2/database';
import {Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {map, switchMap, take} from 'rxjs/operators';
import {IAppUser} from '../models/app-user';
import {TotalItemCountService} from './total-item-count-service';
import {RefreshDataCartService} from './refresh-data-cart.service';

@Injectable()
export class ShoppingCartService {

    constructor(private db: AngularFireDatabase,
                private countItemsService: TotalItemCountService,
                private refreshDataCart: RefreshDataCartService) {
    }

    addToCart(product: IProduct) {
        this.updateItemQty(product, 1);
    }

    removeFromCart(product) {
        this.updateItemQty(product, -1);
    }

    getCart(): Observable<ShoppingCart> {
        const cartId = this.getOrCreateCartId();
        return this.db.object('/shopping-cart/' + cartId)
            .valueChanges()
            .pipe(
                map((cart: any) => {
                    const newShoppingCart = new ShoppingCart(cart.items ? cart.items : []);
                    this.countItemsService.setItemCount(newShoppingCart.totalItemCount);
                    return newShoppingCart;
                })
            );
    }

    clearCardId() {
        this.countItemsService.setItemCount(0);
        localStorage.setItem('cardId', null);
    }

    clearAllCart() {
        const cartId = this.getOrCreateCartId();
        return this.db.object('/shopping-cart/' + cartId + '/items').remove();
    }

    manageShoppingCart(appUser$: Observable<IAppUser>) {
        const cartId = this.getOrCreateCartId();
        this.checkUserInfo(appUser$, cartId);
        this.getCart();
    }

    private checkUserInfo(appUser$: Observable<IAppUser>, cartId: string) {
        let clientCartId;
        appUser$
            .pipe(
                take(1)
            )
            .subscribe((user: IAppUser) => {
                if (user) {
                    clientCartId = user.shoppingCard;
                    if (clientCartId) {
                        if (clientCartId !== cartId) {
                            localStorage.setItem('cardId', clientCartId);
                            this.getCartItems(cartId)
                                .subscribe((items: any) => {
                                    this.removeCart(cartId);
                                    items.map((data: IProduct) => {
                                        this.addToCart(data);
                                    });
                                });
                        }
                    } else {
                        this.updateCartId(appUser$, cartId);
                    }
                }
                this.refreshDataCart.refreshDataCart();
            });
    }

    private getCartItems(cartId: string) {
        return this.db.list(`/shopping-cart/${cartId}/items`)
            .snapshotChanges()
            .pipe(
                map((items) => { // <== new way of chaining
                    return items.map((data: any) => {
                        const product = data.payload.val();
                        const $key = data.payload.key;
                        return {$key, ...product};
                    });
                }),
                take(1)
            );
    }

    private removeCart(cartId: string) {
        this.db.object(`/shopping-cart/${cartId}`).remove();
    }


    private updateItemQty(product: IProduct, change: number) {
        const cartId = this.getOrCreateCartId();
        const item$ = this.getItem(cartId, product.$key);
        item$.valueChanges()
            .pipe(
                take(1)
            )
            .subscribe((item: any) => {
                const updatedQty = (item ? item.qty : 0) + change;
                if (updatedQty <= 0) {
                    item$.remove();
                } else {
                    item$.update({
                        title: product.title,
                        imageUrl: product.imageUrl,
                        price: product.price,
                        qty: updatedQty
                    });
                }
            });
    }

    private getNewCartId(): string {
        return this.db.list('shopping-cart').push({
            dateTime: new Date().getTime()
        }).key;
    }

    private updateCartId(appUser$: Observable<IAppUser>, cartId: string) {
        appUser$.subscribe((user: IAppUser) => {
            this.db.object(`user/${user.$key}`).update({'shoppingCard': cartId});
        });
    }

    private getOrCreateCartId(): string {
        let cartId = localStorage.getItem('cardId');
        if (cartId !== 'null') {
            return cartId;
        }
        cartId = this.getNewCartId();
        localStorage.setItem('cardId', cartId);
        return cartId;
    }

    private getItem(cartId, key) {
        return this.db.object('/shopping-cart/' + cartId + '/items/' + key);
    }

}
