import {AngularFireDatabase} from 'angularfire2/database';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class CategoryService {

    constructor(private db: AngularFireDatabase) {
    }

    getCategories(): Observable<any[]> {
        return this.db.list('/categories')
            .snapshotChanges()
            .pipe(
                map((items) => { // <== new way of chaining
                    return items.map((data: any) => {
                        const category = data.payload.val();
                        const $key = data.payload.key;
                        return {$key, ...category};
                    });
                }));
    }
}
