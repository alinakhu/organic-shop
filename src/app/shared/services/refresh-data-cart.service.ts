import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RefreshDataCartService {

  constructor() { }
    refreshDataCartSource: Subject<any> = new Subject<any>();
    refreshDataCart$: Observable<any> = this.refreshDataCartSource.asObservable();

    refreshDataCart() {
        this.refreshDataCartSource.next();
    }
}
