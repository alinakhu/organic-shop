import {UserService} from './user.service';
import {IAppUser} from '../models/app-user';
import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase';
import {Observable, of} from 'rxjs';
import {switchMap, take} from 'rxjs/operators';
import {RefreshDataCartService} from './refresh-data-cart.service';
import {ShoppingCartService} from './shopping-cart.service';

@Injectable()
export class AuthService {
    user$: Observable<firebase.User>;

    constructor(private userService: UserService,
                private afAuth: AngularFireAuth,
                private shoppingCart: ShoppingCartService) {
        this.user$ = afAuth.authState;
        afAuth.auth.onAuthStateChanged(() => {
                shoppingCart.manageShoppingCart(this.appUser$);
            }
        );
    }

    login(provider: firebase.auth.AuthProvider) {
        this.afAuth.auth.signInWithPopup(provider);
    }

    logout() {
        this.afAuth.auth.signOut();
    }

    get appUser$(): Observable<IAppUser> {
        return this.user$
            .pipe(
                switchMap(user => {
                    if (user) {
                        return this.userService.get(user.uid);
                    }
                    return of(null);
                })
            );
    }

}
