import {AngularFireDatabase} from 'angularfire2/database';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {IProduct} from '../models/product';


@Injectable()
export class ProductService {

    constructor(private db: AngularFireDatabase) {
    }

    create(product) {
        return this.db.list('/products').push(product);
    }

    getAll(): Observable<IProduct[]> {
        return this.db.list('/products', ref => ref.orderByChild('category'))
            .snapshotChanges()
            .pipe(
                map((items) => { // <== new way of chaining
                    return items.map((data: any) => {
                        const product = data.payload.val();
                        const $key = data.payload.key;
                        return {$key, ...product};
                    });
                }));
    }

    getById(id: string) {
        return this.db.object('/products/' + id);
    }

    updateById(id, product) {
        return this.db.object('/products/' + id).update(product);
    }

    deleteById(id) {
        return this.db.object('/products/' + id).remove();
    }
}
