import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
const DEFAULT_ITEM_COUNT = 0;

@Injectable()
export class TotalItemCountService {
    // configService
    itemCountSubject: BehaviorSubject<number> = new BehaviorSubject<number>(DEFAULT_ITEM_COUNT);
    itemCount$: Observable<number> = this.itemCountSubject.asObservable();

    setItemCount(count: number) {
        this.itemCountSubject.next(count);
    }
}
