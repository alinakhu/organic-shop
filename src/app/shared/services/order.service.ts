import {ShoppingCartService} from './shopping-cart.service';
import {AngularFireDatabase} from 'angularfire2/database';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class OrderService {

    constructor(private db: AngularFireDatabase,
                private cartService: ShoppingCartService) {
    }

    placeOrder(order) {
        const result = this.db.list('/order').push(order);
        this.cartService.clearAllCart();
        return result;
    }

    getAllOrders() {
        return this.db.list('/order')
            .snapshotChanges()
            .pipe(
                map((items) => { // <== new way of chaining
                    return items.map((data: any) => {
                        const product = data.payload.val();
                        const $key = data.payload.key;
                        return {$key, ...product};
                    });
                }));;
    }

    getOrderByUser(userId: string): Observable<any> {
        return this.db.list('/order', ref => ref.orderByChild('user/userId').equalTo(userId))
            .snapshotChanges()
            .pipe(
                map((items) => { // <== new way of chaining
                    return items.map((data: any) => {
                        const product = data.payload.val();
                        const $key = data.payload.key;
                        return {$key, ...product};
                    });
                }));
    }

    getOrderById(orderId: string) {
        return this.db.object('/order/' + orderId).valueChanges() as Observable<any>;
    }
}
