export interface IAppUser {
    $key: string;
    name: string;
    email: string;
    shoppingCard: string;
    isAdmin: boolean;
}
