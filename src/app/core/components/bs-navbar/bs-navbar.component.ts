import {Component} from '@angular/core';
import {OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IAppUser} from '../../../shared/models/app-user';
import {ShoppingCartService} from '../../../shared/services/shopping-cart.service';
import {AuthService} from '../../../shared/services/auth.service';
import {TotalItemCountService} from '../../../shared/services/total-item-count-service';
import {RefreshDataCartService} from '../../../shared/services/refresh-data-cart.service';

@Component({
    selector: 'app-bs-navbar',
    templateUrl: './bs-navbar.component.html',
    styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {
    navbarCollapsed: boolean;
    user$: Observable<IAppUser>;
    totalCart$: Observable<number>;

    constructor(private cartService: ShoppingCartService,
                private authService: AuthService,
                public countItemsService$: TotalItemCountService,
                private shoppingCartService: ShoppingCartService) {
    }

    ngOnInit() {
        this.shoppingCartService.getCart();
        this.user$ = this.authService.appUser$;
        this.totalCart$ = this.countItemsService$.itemCount$;
    }

    logout() {
        this.authService.logout();
        this.cartService.clearCardId();
    }
}
