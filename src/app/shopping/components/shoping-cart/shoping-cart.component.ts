import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ShoppingCartService} from '../../../shared/services/shopping-cart.service';
import {ShoppingCart} from '../../../shared/models/shopping-cart';

@Component({
    selector: 'app-shoping-cart',
    templateUrl: './shoping-cart.component.html',
    styleUrls: ['./shoping-cart.component.css']
})
export class ShopingCartComponent implements OnInit {
    cart$: Observable<ShoppingCart>;

    constructor(private cartService: ShoppingCartService) {
    }

    ngOnInit() {
        this.cart$ = this.cartService.getCart();
    }

    clearAll() {
        this.cartService.clearAllCart();
    }
}
