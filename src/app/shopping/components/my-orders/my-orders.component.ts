import {OnInit, OnDestroy, Component} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from '../../../shared/services/auth.service';
import {OrderService} from '../../../shared/services/order.service';
import {switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-my-orders',
    templateUrl: './my-orders.component.html',
    styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {

    order$: Observable<any[]>;

    constructor(
        private auth: AuthService,
        private orderService: OrderService) {
    }

    ngOnInit() {
        this.order$ = this.auth.user$
            .pipe(
                switchMap(user => this.orderService.getOrderByUser(user.uid))
            );
        this.order$.subscribe(data => {
            console.log(data);
        });
    }
}
