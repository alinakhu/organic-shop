import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IProduct} from '../../../shared/models/product';
import {ShoppingCart} from '../../../shared/models/shopping-cart';
import {ShoppingCartService} from '../../../shared/services/shopping-cart.service';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../shared/services/product.service';
import {switchMap} from 'rxjs/operators';
import {RefreshDataCartService} from '../../../shared/services/refresh-data-cart.service';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

    products: IProduct[] = [];
    filteredProducts: IProduct[];
    category: string;
    cart$: Observable<ShoppingCart>;

    constructor(
        private cartService: ShoppingCartService,
        private route: ActivatedRoute,
        private productService: ProductService,
        private refreshDataCart: RefreshDataCartService
    ) {
    }

    ngOnInit() {
        this.cart$ = this.cartService.getCart();
        this.refreshDataCart.refreshDataCart$
            .subscribe(() => {
                this.cart$ = this.cartService.getCart();
        });
        this.populateProducts();
    }

    private populateProducts() {
        this.productService.getAll()
            .pipe(
                switchMap((products: IProduct[]) => {
                    this.products = products;
                    return this.route.queryParamMap;
                })
            )
            .subscribe((params: any) => {
                this.category = params.get('category');
                this.applyFilter();
            });
    }

    private applyFilter() {
        this.filteredProducts = !this.category ? this.products
            : this.products.filter(e => e.category === this.category);
    }


}
