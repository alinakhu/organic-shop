import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { AngularFireModule } from 'angularfire2';

import {AppComponent} from './app.component';
import {environment} from '../environments/environment';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {AdminModule} from './admin/admin.module';
import {ShoppingModule} from './shopping/shopping.module';
import {RouterModule} from '@angular/router';
import {ProductsComponent} from './shopping/components/products/products.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase),
        CoreModule,
        SharedModule,
        AdminModule,
        ShoppingModule,
        RouterModule.forRoot([
            { path: '', component: ProductsComponent }
        ])
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
